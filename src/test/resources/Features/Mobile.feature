@mobile
Feature: Play music on the uamp App

  Scenario Outline: Play music on the uamp App
    Given User Launch UAMP app
    And User Clicks on the menu icon located at the top right
    And User clicks on All music option
    And User clicks on genres option
    And User clicks on genre "<Genre>"
    When User selects song "<Song>"
    Then The music is played on the app
    And User pause music

    Examples:
      |Genre|Song|
      |Rock|Drop and Roll|
