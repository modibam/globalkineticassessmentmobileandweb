@web
Feature: Job Application

  Scenario Outline: Navigate to Job link
    Given User opens browser "<Browser>" and launches Web Address "https://www.ilabquality.com"
    And User selects hyperlink Careers
    And User Selects Country name "<Country>"
    And User clicks first available post
    And User clicks on Apply online button
    And The Applicant name "<Names>"
    And Applicant Email address "<Email>"
    And Phone number
    When User click on Send Application button
    Then Error message displayed "<ErrorMessage>"
    Examples:
 |Browser|Country|Names|Email|ErrorMessage|
 |CHROME|South Africa|Morongwa Modiba|automationAssessment@gmail.com|You need to upload at least one file.|
 |CHROME|South Africa|Test Testing|automationAssessment@gmail.com|You need to upload at least one file.|

