package TestSteps;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class APIRequests {
    Response response = null;
    String endpoint  = null;
    @Given("^End point \"([^\"]*)\"$")
    public void end_point(String arg1) throws Throwable {
        endpoint = arg1;


    }

    @When("^a request is send to the server$")
    public void a_request_is_send_to_the_server() throws Throwable {
        RestAssured.baseURI = endpoint;


        response = RestAssured.given()
                //   .contentType(ContentType.JSON)
                .when()
                .get();

    }

    @Then("^Response is received with status (\\d+)$")
    public void response_is_received_with_status(int arg1) throws Throwable {
             assertEquals(arg1, response.getStatusCode());
    }

    @And("^list of all breeds is displayed$")
    public void list_of_all_breeds_is_displayed() throws Throwable {
        System.out.println(response.asString());
    }

    @Then("^\"([^\"]*)\" subbreed list is displayed\\.$")
    public void subbreed_list_is_displayed(String arg1) throws Throwable {
        Assert.assertTrue(response.body().jsonPath().get("message").toString().contains(arg1));
    }
    @Then("^\"([^\"]*)\" subbreed random image is displayed\\.$")
    public void subbreed_random_image_is_displayed(String arg1) throws Throwable {
        Assert.assertTrue(response.body().jsonPath().get("message").toString().contains(arg1));
    }
}
