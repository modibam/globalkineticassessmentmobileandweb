package TestSteps;

import CommonUtils.Constant;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.asserts.SoftAssert;
import java.net.URL;

public class mobile {
    static WebDriver driver;
    @Given("^User Launch UAMP app$")
    public void user_Launch_UAMP_app() throws Throwable {
        SoftAssert sa = new SoftAssert();
        DesiredCapabilities caps = new DesiredCapabilities();
        //caps.setCapability("deviceName", "My Phone");
        //   caps.setCapability("udid", "WUJ01RKDVQ"); //Give Device ID of your mobile phone
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "10");
        caps.setCapability("appPackage", "com.instantappsample.uamp");
        caps.setCapability("appActivity", "com.example.android.uamp.ui.MusicPlayerActivity");
        caps.setCapability("noReset", "true");
        //  caps.setCapability("automationName", "UiAutomator1");
        caps.setCapability("autoGrantPermissions", true);
        //   caps.acceptInsecureCerts();
        caps.setCapability("noSign", "true");


        try {
            //   repo.setExtentTest(repo.getExtent().createTest("Launch Africa Private Equity Mobile App "));

            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),caps);
           AppiumDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
            Constant.driver = driver;
            //       Thread.sleep(10000);

          /*  if (driver.findElement(By.xpath("//android.view.View[@content-desc='Home']/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")).isDisplayed())
            {
                //   repo.ExtentLogPass("Launch successful",true,utils);
                sa.assertEquals("Launch", "Launch");
            }*/
        } catch (Exception e) {
            System.out.println(e);
       //     sa.assertEquals("Fail", "Launch");
        }
      //  sa.assertAll();
    }

    @Given("^User Clicks on the menu icon located at the top right$")
    public void user_Clicks_on_the_menu_icon_located_at_the_top_right() throws Throwable {
        Thread.sleep(5000);

        Constant.driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Open the main menu\"]")).click();
    }

    @Given("^User clicks on All music option$")
    public void user_clicks_on_All_music_option() throws Throwable {
        Constant.driver.findElement(By.xpath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[1]/android.widget.CheckedTextView")).click();

    }

    @Given("^User clicks on genres option$")
    public void user_clicks_on_genres_option() throws Throwable {
        Thread.sleep(5000);
        Constant.driver.findElement(By.xpath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]")).click();
    }

    @Given("^User clicks on genre \"([^\"]*)\"$")
    public void user_clicks_on_genre(String arg1) throws Throwable {

        if(arg1.equalsIgnoreCase("Rock")) {
            Constant.driver.findElement(By.xpath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView[1]")).click();
        }
        else if(arg1.equalsIgnoreCase("Cinematic"))
        {
            Constant.driver.findElement(By.xpath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.TextView[1]")).click();
        }
        else
        {
            Constant.driver.findElement(By.xpath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[3]/android.widget.LinearLayout/android.widget.TextView[1]")).click();
        }


    }

    @When("^User selects song \"([^\"]*)\"$")
    public void user_selects_song(String arg1) throws Throwable {
        Constant.driver.findElement(By.xpath("(//android.widget.ImageView[@content-desc=\"Play item\"])[6]")).click();
    }

    @Then("^The music is played on the app$")
    public void the_music_is_played_on_the_app() throws Throwable {
        Constant.driver.findElement((By.id("com.instantappsample.uamp:id/play_pause"))).click();
    }

    @Then("^User pause music$")
    public void user_pause_music() throws Throwable {
        Constant.driver.findElement((By.id("com.instantappsample.uamp:id/play_pause"))).click();
        Constant.driver.quit();
    }



}
