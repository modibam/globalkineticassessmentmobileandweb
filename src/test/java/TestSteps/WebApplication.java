package TestSteps;

import CommonUtils.Constant;
import CommonUtils.launchUrl;
import CommonUtils.phoneNo;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;

public class WebApplication {
    WebDriver driver = null;

    @Given("^User opens browser \"([^\"]*)\" and launches Web Address \"([^\"]*)\"$")
    public void user_launches_Web_Address(String strBrowser,String Url) throws Throwable {
        launchUrl.execute(strBrowser,Url);
        driver = Constant.driver;
    }

    @And("^User selects hyperlink Careers$")
    public void user_selects_hyperlink() throws Throwable {
        Constant.driver.findElement(By.xpath("//a[contains(text(),'CAREERS')]")).click();

    }

    @And("^User Selects Country name \"([^\"]*)\"$")
    public void user_Selects_Country_name(String country) throws Throwable {
           //descriptive example
        driver.findElement(By.xpath("//a[contains(text(),'"+country+"')]")).click();

    }

    @And("^User clicks first available post$")
    public void user_clicks_first_available_post() throws Throwable {
        Constant.driver.findElement(By.xpath("//a[contains(text(),'Software Test Engineer - Johannesburg')]")).click();


    }

    @And("^User clicks on Apply online button$")
    public void user_clicks_on_Apply_online_button() throws Throwable {
        Constant.driver.findElement(By.xpath("//a[contains(text(),'Apply Online')]")).click();
    }

    @And("^The Applicant name \"([^\"]*)\"$")
    public void the_Applicant_name(String appName) throws Throwable {
        Constant.driver.findElement(By.xpath("//*[@id=\"applicant_name\"]")).sendKeys(appName);

    }

    @And("^Applicant Email address \"([^\"]*)\"$")
    public void applicant_Email_adress(String emailAddr) throws Throwable {
        Constant.driver.findElement(By.xpath("//input[@id='email']")).sendKeys(emailAddr);
    }

    @And("^Phone number$")
    public void phone_number() throws Throwable {
       String cellNo = String.valueOf(phoneNo.phonegenerate());
        Constant.driver.findElement(By.xpath("//input[@id='phone']")).sendKeys(cellNo);
    }

    @When("^User click on Send Application button$")
    public void user_click_on_Send_Application_button() throws Throwable {
        Constant.driver.findElement(By.xpath("//input[@id='wpjb_submit']")).click();
    }

    @Then("^Error message displayed \"([^\"]*)\"$")
    public void error_message_displayed(String errMsg) throws Throwable {
      String errorMsg =  Constant.driver.findElement(By.xpath("//form[@id='wpjb-apply-form']/fieldset/div[5]/div/ul/li")).getAttribute("innerText");
        assertEquals(errMsg,errorMsg);
        driver.close();
    }



}
