package CommonUtils;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class launchUrl {

    public static WebDriver execute(String strBrowser,String strUrl) throws Exception
    {
    //    String strURL = DriverSheet.getRow(jintRow).getCell(5).getStringCellValue();
    //    String strBrowser = DriverSheet.getRow(jintRow).getCell(3).getStringCellValue();
        WebDriver driver = null;
    //    strBrowser = strBrowser.trim().toUpperCase();
        DesiredCapabilities oCap;


            switch (strBrowser.toUpperCase())
            {
                case "CHROME":

                    System.setProperty("webdriver.chrome.driver", "src/test/resources/Plugins/chromedriver.exe");
                    System.out.println(System.getProperty("webdriver.chrome.driver"));
                    driver=new ChromeDriver();
                    break;
                case "FIREFOX":
                    System.setProperty("webdriver.gecko.driver", "src/test/resources/Plugins/chromedriver.exe");
                    DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                    capabilities.setCapability("marionette",true);
                    driver= new FirefoxDriver(capabilities);
                 /*   System.setProperty("webdriver.gecko.driver", "src/test/resources/Plugins/chromedriver.exe");
                    driver = new FirefoxDriver();*/
                    break;
                case "IE":
                    System.setProperty("webdriver.ie.driver", "src/test/resources/Plugins/IEDriverServer.exe");
                    oCap=DesiredCapabilities.internetExplorer();
                    oCap.setCapability("ignoreZoomSetting", true);
                    oCap.setCapability(CapabilityType.BROWSER_NAME, UnexpectedAlertBehaviour.IGNORE);
                    driver = new InternetExplorerDriver(oCap);
                    break;

                default:
                    break;

            }
            driver.get(strUrl);
        Constant.driver = driver;

        return driver;
    }







}