how the framework was designed
Install DK on the machine
Map the Jdk in the system environment variables by adding the Jdk installation path and the bin folder
Created a maven project 
Added dependencies to the pom file
Used the cucumber BDD framework, installed cucumber plugin

Mobile Application
Installed Appium server to identify object properties

Please Note
You can use the emulator with UAMP application installed to run the mobile test or use the physical android phone with UAMP application installed on it and the physical phone will need the cable to connect to the laptop

To Run the test
The feature files can be run independently by right clicking and selecting run feature file
Also the scenarios can be run independently by right clicking a scenario and selecting run scenario
The test can also be run from the “Runner” file located under srs/test/Java/RunnerClass/Runner. To run right click the runner file and select Run


Reporting
A report will be generated after running the tests under: target\ExtentReport\report.tml
The project was uploaded to gitlab under the link: https://gitlab.com/modibam/globalkineticassessmentmobileandweb


